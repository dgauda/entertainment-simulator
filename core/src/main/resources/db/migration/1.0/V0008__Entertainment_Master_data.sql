INSERT INTO Entertainment(id, modificationCounter, address, zip, entertainmenttype) VALUES (1, 0, 'Mumbai', 400076, 'Adventure');
INSERT INTO Entertainment(id, modificationCounter, address, zip, entertainmenttype) VALUES (2, 0, 'Mumbai', 400079, 'Horror');

INSERT INTO Entertainment_Showdetails(id, modificationCounter, classtype, showtime, entertainmentid) VALUES (1, 0, 'Gold', '2018-11-03 17:00:00', 1);
INSERT INTO Entertainment_Showdetails(id, modificationCounter, classtype, showtime, entertainmentid) VALUES (2, 0, 'Silver', '2018-10-25 15:00:00', 2);
INSERT INTO Entertainment_Showdetails(id, modificationCounter, classtype, showtime, entertainmentid) VALUES (3, 0, 'Gold', '2018-10-25 16:00:00', 2);

INSERT INTO Entertainment_Customer(id, modificationCounter, name, contactnumber, isregularcustomer, registrationdate) VALUES (1, 0, 'Mark', '3242424', 'Y', '2018-10-11');
INSERT INTO Entertainment_Customer(id, modificationCounter, name, contactnumber, isregularcustomer, registrationdate) VALUES (1001, 0, 'John', '123123', 'Y', '2019-02-12');


