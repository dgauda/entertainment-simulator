	CREATE TABLE Entertainment (
		id BIGINT NOT NULL AUTO_INCREMENT,
		modificationCounter INTEGER NOT NULL, 
		address varchar(100),  
		zip integer, 
		entertainmenttype VARCHAR(255),
		CONSTRAINT PK_Entertainment PRIMARY KEY(id)
	);
	
	CREATE TABLE Entertainment_Customer ( 
		id BIGINT NOT NULL AUTO_INCREMENT,
		modificationCounter INTEGER NOT NULL, 
		name VARCHAR(255), 
		contactnumber integer, 
		isregularcustomer boolean, 
		registrationDate TIMESTAMP,
		CONSTRAINT PK_Entertainment_Customer PRIMARY KEY(id)
	);
	
	CREATE TABLE Entertainment_Showdetails (
		id BIGINT NOT NULL AUTO_INCREMENT,
		modificationCounter INTEGER NOT NULL, 
		classtype VARCHAR(255), 
		showtime TIMESTAMP, 
		entertainmentid BIGINT NOT NULL,
		CONSTRAINT PK_Entertainment_Showdetails PRIMARY KEY(id),
		CONSTRAINT FK_Entertainment_Showdetails_entertainmentid FOREIGN KEY(entertainmentid) REFERENCES Entertainment(id) NOCHECK
	);
	
	CREATE TABLE Entertainment_Reservation (
		id BIGINT NOT NULL AUTO_INCREMENT,
		modificationCounter INTEGER NOT NULL, 
		bookingdate DATE, 
		nooftickets INTEGER, 
		showid BIGINT NOT NULL,
		customerid BIGINT NOT NULL,
		CONSTRAINT PK_Entertainment_Reservation PRIMARY KEY(id),
		CONSTRAINT FK_Entertainment_Reservation_showid FOREIGN KEY(showid) REFERENCES Entertainment_Showdetails(id) NOCHECK,
		CONSTRAINT FK_Entertainment_Reservation_customerid FOREIGN KEY(customerid) REFERENCES Entertainment_Customer(id) NOCHECK
	);
		
		
		