    CREATE TABLE ParkingLot ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,     
      address VARCHAR(1000),
      latitude DOUBLE,
      longitude DOUBLE,
      totalNoOfSlots BIGINT,
      availableNoOfSlots BIGINT,
      operatingcompany VARCHAR(1000),
      CONSTRAINT PK_ParkingLot PRIMARY KEY(id)     
    );  

    CREATE TABLE ParkingSlotStatus ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      name VARCHAR(255),
      desc VARCHAR(2000),
      CONSTRAINT PK_ParkingSlotStatus PRIMARY KEY(id)      
    );
    
    CREATE TABLE ParkingSlot ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      slotNumber BIGINT AUTO_INCREMENT,
      slotStatus VARCHAR(100),
      idParkingLot BIGINT NOT NULL,
      idParkingSlotStatus BIGINT NOT NULL,
      CONSTRAINT PK_ParkingSlot PRIMARY KEY(id),
      CONSTRAINT FK_ParkingSlot_idParkingLot FOREIGN KEY(idParkingLot) REFERENCES ParkingLot(id) NOCHECK,
      CONSTRAINT FK_ParkingSlot_idParkingSlotStatus FOREIGN KEY(idParkingSlotStatus) REFERENCES ParkingSlotStatus(id) NOCHECK
    );    
    

    CREATE TABLE Reservation ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      idParkingLot BIGINT,
      idParkingSlot BIGINT,
      itineraryId BIGINT,
      startTimestamp TIMESTAMP,
      endTimestamp TIMESTAMP,
      CONSTRAINT PK_Reservation PRIMARY KEY(id),
      CONSTRAINT FK_Reservation_idParkingLot FOREIGN KEY(idParkingLot) REFERENCES ParkingLot(id) NOCHECK,
      CONSTRAINT FK_Reservation_idParkingSlot FOREIGN KEY(idParkingSlot) REFERENCES ParkingSlot(id) NOCHECK
    );
    
    