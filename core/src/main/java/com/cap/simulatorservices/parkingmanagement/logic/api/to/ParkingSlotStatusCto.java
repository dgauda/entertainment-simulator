package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of ParkingSlotStatus
 */
public class ParkingSlotStatusCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ParkingSlotStatusEto parkingSlotStatus;

  public ParkingSlotStatusEto getParkingSlotStatus() {

    return parkingSlotStatus;
  }

  public void setParkingSlotStatus(ParkingSlotStatusEto parkingSlotStatus) {

    this.parkingSlotStatus = parkingSlotStatus;
  }

}
