package com.cap.simulatorservices.parkingmanagement.dataaccess.impl.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ReservationEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ReservationDao;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ReservationDao}.
 */
@Named
public class ReservationDaoImpl extends ApplicationDaoImpl<ReservationEntity> implements ReservationDao {

  /**
   * The constructor.
   */
  public ReservationDaoImpl() {

    super();
  }

  @Override
  public Class<ReservationEntity> getEntityClass() {

    return ReservationEntity.class;
  }

  @Override
  public PaginatedListTo<ReservationEntity> findReservations(ReservationSearchCriteriaTo criteria) {

    ReservationEntity reservation = Alias.alias(ReservationEntity.class);
    EntityPathBase<ReservationEntity> alias = Alias.$(reservation);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    Long itineraryId = criteria.getItineraryId();
    if (itineraryId != null) {
      query.where(Alias.$(reservation.getItineraryId()).eq(itineraryId));
    }
    Timestamp startTimestamp = criteria.getStartTimestamp();
    if (startTimestamp != null) {
      query.where(Alias.$(reservation.getStartTimestamp()).eq(startTimestamp));
    }
    Timestamp endTimestamp = criteria.getEndTimestamp();
    if (endTimestamp != null) {
      query.where(Alias.$(reservation.getEndTimestamp()).eq(endTimestamp));
    }
    Long parkingLotId = criteria.getParkingLotId();
    if (parkingLotId != null) {
      if (reservation.getParkingLot() != null) {
        query.where(Alias.$(reservation.getParkingLot().getId()).eq(parkingLotId));
      }
    }
    addOrderBy(query, alias, reservation, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ReservationEntity> alias, ReservationEntity reservation,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "itineraryId":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(reservation.getItineraryId()).asc());
            } else {
              query.orderBy(Alias.$(reservation.getItineraryId()).desc());
            }
            break;
          case "startTimestamp":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(reservation.getStartTimestamp()).asc());
            } else {
              query.orderBy(Alias.$(reservation.getStartTimestamp()).desc());
            }
            break;
          case "endTimestamp":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(reservation.getEndTimestamp()).asc());
            } else {
              query.orderBy(Alias.$(reservation.getEndTimestamp()).desc());
            }
            break;
          case "parkingLotDetails":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(reservation.getParkingLot().getId()).asc());
            } else {
              query.orderBy(Alias.$(reservation.getParkingLot().getId()).desc());
            }
            break;
        }
      }
    }
  }

}