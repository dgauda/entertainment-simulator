package com.cap.simulatorservices.parkingmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ParkingSlotStatus extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public String getDesc();

  public void setDesc(String desc);

}
