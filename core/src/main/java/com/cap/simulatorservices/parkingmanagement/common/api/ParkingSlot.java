package com.cap.simulatorservices.parkingmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ParkingSlot extends ApplicationEntity {

  public Long getSlotNumber();

  public void setSlotNumber(Long slotNumber);

  public Long getParkingLotId();

  public void setParkingLotId(Long parkingLotDetailsId);

}
