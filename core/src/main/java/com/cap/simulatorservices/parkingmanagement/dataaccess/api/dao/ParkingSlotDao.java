package com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ParkingSlot entities
 */
public interface ParkingSlotDao extends ApplicationDao<ParkingSlotEntity> {

  /**
   * Finds the {@link ParkingSlotEntity parkingslots} matching the given {@link ParkingSlotSearchCriteriaTo}.
   *
   * @param criteria is the {@link ParkingSlotSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ParkingSlotEntity} objects.
   */
  PaginatedListTo<ParkingSlotEntity> findParkingSlots(ParkingSlotSearchCriteriaTo criteria);

}
