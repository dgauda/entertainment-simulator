package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.simulatorservices.parkingmanagement.common.api.ParkingSlot}s.
 *
 */
public class ParkingSlotSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long slotNumber;

  private Long parkingLotDetailsId;

  /**
   * The constructor.
   */
  public ParkingSlotSearchCriteriaTo() {

    super();
  }

  public Long getSlotNumber() {

    return this.slotNumber;
  }

  public void setSlotNumber(Long slotNumber) {

    this.slotNumber = slotNumber;
  }

  public Long getParkingLotDetailsId() {

    return this.parkingLotDetailsId;
  }

  public void setParkingLotDetailsId(Long parkingLotDetailsId) {

    this.parkingLotDetailsId = parkingLotDetailsId;
  }

}
