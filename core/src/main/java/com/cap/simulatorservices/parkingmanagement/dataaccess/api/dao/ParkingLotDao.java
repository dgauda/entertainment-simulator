package com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingLotEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ParkingLot entities
 */
public interface ParkingLotDao extends ApplicationDao<ParkingLotEntity> {

  /**
   * Finds the {@link ParkingLotEntity parkinglots} matching the given {@link ParkingLotSearchCriteriaTo}.
   *
   * @param criteria is the {@link ParkingLotSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ParkingLotEntity} objects.
   */
  PaginatedListTo<ParkingLotEntity> findParkingLots(ParkingLotSearchCriteriaTo criteria);
}
