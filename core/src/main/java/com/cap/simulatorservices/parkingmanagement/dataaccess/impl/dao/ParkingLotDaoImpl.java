package com.cap.simulatorservices.parkingmanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingLotEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ParkingLotDao;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ParkingLotDao}.
 */
@Named
public class ParkingLotDaoImpl extends ApplicationDaoImpl<ParkingLotEntity> implements ParkingLotDao {

  /**
   * The constructor.
   */
  public ParkingLotDaoImpl() {

    super();
  }

  @Override
  public Class<ParkingLotEntity> getEntityClass() {

    return ParkingLotEntity.class;
  }

  @Override
  public PaginatedListTo<ParkingLotEntity> findParkingLots(ParkingLotSearchCriteriaTo criteria) {

    ParkingLotEntity parkinglot = Alias.alias(ParkingLotEntity.class);
    EntityPathBase<ParkingLotEntity> alias = Alias.$(parkinglot);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String address = criteria.getAddress();
    if (address != null) {
      query.where(Alias.$(parkinglot.getAddress()).eq(address));
    }
    Double latitude = criteria.getLatitude();
    if (latitude != null) {
      query.where(Alias.$(parkinglot.getLatitude()).eq(latitude));
    }
    Double longitude = criteria.getLongitude();
    if (longitude != null) {
      query.where(Alias.$(parkinglot.getLongitude()).eq(longitude));
    }
    Long totalNoOfSlots = criteria.getTotalNoOfSlots();
    if (totalNoOfSlots != null) {
      query.where(Alias.$(parkinglot.getTotalNoOfSlots()).eq(totalNoOfSlots));
    }
    Long availableNoOfSlots = criteria.getAvailableNoOfSlots();
    if (availableNoOfSlots != null) {
      query.where(Alias.$(parkinglot.getAvailableNoOfSlots()).eq(availableNoOfSlots));
    }
    String operatingcompany = criteria.getOperatingcompany();
    if (operatingcompany != null) {
      query.where(Alias.$(parkinglot.getOperatingcompany()).eq(operatingcompany));
    }
    addOrderBy(query, alias, parkinglot, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ParkingLotEntity> alias, ParkingLotEntity parkinglot,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "address":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkinglot.getAddress()).asc());
            } else {
              query.orderBy(Alias.$(parkinglot.getAddress()).desc());
            }
            break;
          case "latitude":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkinglot.getLatitude()).asc());
            } else {
              query.orderBy(Alias.$(parkinglot.getLatitude()).desc());
            }
            break;
          case "longitude":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkinglot.getLongitude()).asc());
            } else {
              query.orderBy(Alias.$(parkinglot.getLongitude()).desc());
            }
            break;
          case "totalNoOfSlots":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkinglot.getTotalNoOfSlots()).asc());
            } else {
              query.orderBy(Alias.$(parkinglot.getTotalNoOfSlots()).desc());
            }
            break;
          case "availableNoOfSlots":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkinglot.getAvailableNoOfSlots()).asc());
            } else {
              query.orderBy(Alias.$(parkinglot.getAvailableNoOfSlots()).desc());
            }
            break;
          case "operatingcompany":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkinglot.getOperatingcompany()).asc());
            } else {
              query.orderBy(Alias.$(parkinglot.getOperatingcompany()).desc());
            }
            break;
        }
      }
    }
  }

}