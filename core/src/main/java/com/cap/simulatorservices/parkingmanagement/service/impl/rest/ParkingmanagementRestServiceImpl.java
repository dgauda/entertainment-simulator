package com.cap.simulatorservices.parkingmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import com.cap.simulatorservices.parkingmanagement.logic.api.Parkingmanagement;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.service.api.rest.ParkingmanagementRestService;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Parkingmanagement}.
 */
@Named("ParkingmanagementRestService")
public class ParkingmanagementRestServiceImpl implements ParkingmanagementRestService {

  @Inject
  private Parkingmanagement parkingmanagement;

  @Override
  public ParkingLotEto getParkingLot(long id) {

    return this.parkingmanagement.findParkingLot(id);
  }

  @Override
  public ParkingLotEto saveParkingLot(ParkingLotEto parkinglot) {

    return this.parkingmanagement.saveParkingLot(parkinglot);
  }

  @Override
  public void deleteParkingLot(long id) {

    this.parkingmanagement.deleteParkingLot(id);
  }

  @Override
  public PaginatedListTo<ParkingLotEto> findParkingLotsByPost(ParkingLotSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingLotEtos(searchCriteriaTo);
  }

  @Override
  public ParkingLotCto getParkingLotCto(long id) {

    return this.parkingmanagement.findParkingLotCto(id);
  }

  @Override
  public PaginatedListTo<ParkingLotCto> findParkingLotCtosByPost(ParkingLotSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingLotCtos(searchCriteriaTo);
  }

  @Override
  public ParkingSlotEto getParkingSlot(long id) {

    return this.parkingmanagement.findParkingSlot(id);
  }

  @Override
  public ParkingSlotEto saveParkingSlot(ParkingSlotEto parkingslot) {

    return this.parkingmanagement.saveParkingSlot(parkingslot);
  }

  @Override
  public void deleteParkingSlot(long id) {

    this.parkingmanagement.deleteParkingSlot(id);
  }

  @Override
  public PaginatedListTo<ParkingSlotEto> findParkingSlotsByPost(ParkingSlotSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingSlotEtos(searchCriteriaTo);
  }

  @Override
  public ParkingSlotCto getParkingSlotCto(long id) {

    return this.parkingmanagement.findParkingSlotCto(id);
  }

  @Override
  public PaginatedListTo<ParkingSlotCto> findParkingSlotCtosByPost(ParkingSlotSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingSlotCtos(searchCriteriaTo);
  }

  @Override
  public ReservationEto getReservation(long id) {

    return this.parkingmanagement.findReservation(id);
  }

  @Override
  public ReservationEto saveReservation(ReservationEto reservation) {

    return this.parkingmanagement.saveReservation(reservation);
  }

  @Override
  public void deleteReservation(long id) {

    this.parkingmanagement.deleteReservation(id);
  }

  @Override
  public PaginatedListTo<ReservationEto> findReservationsByPost(ReservationSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findReservationEtos(searchCriteriaTo);
  }

  @Override
  public ReservationCto getReservationCto(long id) {

    return this.parkingmanagement.findReservationCto(id);
  }

  @Override
  public PaginatedListTo<ReservationCto> findReservationCtosByPost(ReservationSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findReservationCtos(searchCriteriaTo);
  }

  @Override
  public ParkingSlotStatusEto getParkingSlotStatus(long id) {

    return this.parkingmanagement.findParkingSlotStatus(id);
  }

  @Override
  public ParkingSlotStatusEto saveParkingSlotStatus(ParkingSlotStatusEto parkingslotstatus) {

    return this.parkingmanagement.saveParkingSlotStatus(parkingslotstatus);
  }

  @Override
  public void deleteParkingSlotStatus(long id) {

    this.parkingmanagement.deleteParkingSlotStatus(id);
  }

  @Override
  public PaginatedListTo<ParkingSlotStatusEto> findParkingSlotStatussByPost(
      ParkingSlotStatusSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingSlotStatusEtos(searchCriteriaTo);
  }

  @Override
  public ParkingSlotStatusCto getParkingSlotStatusCto(long id) {

    return this.parkingmanagement.findParkingSlotStatusCto(id);
  }

  @Override
  public PaginatedListTo<ParkingSlotStatusCto> findParkingSlotStatusCtosByPost(
      ParkingSlotStatusSearchCriteriaTo searchCriteriaTo) {

    return this.parkingmanagement.findParkingSlotStatusCtos(searchCriteriaTo);
  }

}
