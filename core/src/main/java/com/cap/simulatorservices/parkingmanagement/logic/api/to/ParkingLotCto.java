package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of ParkingLot
 */
public class ParkingLotCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ParkingLotEto parkingLot;

  public ParkingLotEto getParkingLot() {

    return parkingLot;
  }

  public void setParkingLot(ParkingLotEto parkingLot) {

    this.parkingLot = parkingLot;
  }

}
