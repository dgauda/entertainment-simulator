package com.cap.simulatorservices.parkingmanagement.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface Reservation extends ApplicationEntity {

  public Long getItineraryId();

  public void setItineraryId(Long itineraryId);

  public Timestamp getStartTimestamp();

  public void setStartTimestamp(Timestamp startTimestamp);

  public Long getParkingLotId();

  public void setParkingLotId(Long parkingLotId);

  public Long getParkingSlotId();

  public void setParkingSlotId(Long parkingSlotId);
}
