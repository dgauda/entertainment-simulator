package com.cap.simulatorservices.general.dataaccess.api.dao;

import com.cap.simulatorservices.general.dataaccess.api.BinaryObjectEntity;

/**
 * {@link ApplicationDao Data Access Object} for {@link BinaryObjectEntity} entity.
 */
public interface BinaryObjectDao extends ApplicationDao<BinaryObjectEntity> {

}
