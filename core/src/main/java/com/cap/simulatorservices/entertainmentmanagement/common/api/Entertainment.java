package com.cap.simulatorservices.entertainmentmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface Entertainment extends ApplicationEntity {

	public String getAddress();

	public void setAddress(String address);

	public Integer getZip();

	public void setZip(Integer zip);

	public String getEntertainmentType();

	public void setEntertainmentType(String entertainmentType);

}
