package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria}
 * {@link net.sf.mmm.util.transferobject.api.TransferObject TO} used to find
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.Entertainment}s.
 *
 */
public class EntertainmentSearchCriteriaTo extends SearchCriteriaTo {

	private static final long serialVersionUID = 1L;

	private String address;
	private Integer zip;
	private String entertainmentType;

	/**
	 * The constructor.
	 */
	public EntertainmentSearchCriteriaTo() {

		super();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getZip() {
		return zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public String getEntertainmentType() {
		return entertainmentType;
	}

	public void setEntertainmentType(String entertainmentType) {
		this.entertainmentType = entertainmentType;
	}

}
