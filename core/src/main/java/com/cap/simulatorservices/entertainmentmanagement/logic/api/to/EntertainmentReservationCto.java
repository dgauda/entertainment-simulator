package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Reservation
 */
public class EntertainmentReservationCto extends AbstractCto {

	private static final long serialVersionUID = 1L;

	private EntertainmentReservationEto reservation;

	private CustomerEto customerid;
	private ShowDetailsEto showid;

	public EntertainmentReservationEto getReservation() {
		return reservation;
	}

	public void setReservation(EntertainmentReservationEto reservation) {
		this.reservation = reservation;
	}

	public CustomerEto getCustomerid() {
		return customerid;
	}

	public void setCustomerid(CustomerEto customerid) {
		this.customerid = customerid;
	}

	public ShowDetailsEto getShowid() {
		return showid;
	}

	public void setShowid(ShowDetailsEto showid) {
		this.showid = showid;
	}

}
