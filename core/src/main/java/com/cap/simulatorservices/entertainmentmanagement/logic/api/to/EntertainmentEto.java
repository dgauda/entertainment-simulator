package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.cap.simulatorservices.entertainmentmanagement.common.api.Entertainment;
import com.cap.simulatorservices.general.common.api.to.AbstractEto;

/**
 * Entity transport object of Entertainment
 */
public class EntertainmentEto extends AbstractEto implements Entertainment {

	private static final long serialVersionUID = 1L;

	private String address;
	private Integer zip;
	private String entertainmentType;

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public Integer getZip() {
		return zip;
	}

	@Override
	public void setZip(Integer zip) {
		this.zip = zip;
	}

	@Override
	public String getEntertainmentType() {
		return entertainmentType;
	}

	@Override
	public void setEntertainmentType(String entertainmentType) {
		this.entertainmentType = entertainmentType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
		result = prime * result + ((this.zip == null) ? 0 : this.zip.hashCode());
		result = prime * result + ((this.entertainmentType == null) ? 0 : this.entertainmentType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		// class check will be done by super type EntityTo!
		if (!super.equals(obj)) {
			return false;
		}
		EntertainmentEto other = (EntertainmentEto) obj;
		if (this.address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!this.address.equals(other.address)) {
			return false;
		}
		if (this.zip == null) {
			if (other.zip != null) {
				return false;
			}
		} else if (!this.zip.equals(other.zip)) {
			return false;
		}
		if (this.entertainmentType == null) {
			if (other.entertainmentType != null) {
				return false;
			}
		} else if (!this.entertainmentType.equals(other.entertainmentType)) {
			return false;
		}
		return true;
	}
}
