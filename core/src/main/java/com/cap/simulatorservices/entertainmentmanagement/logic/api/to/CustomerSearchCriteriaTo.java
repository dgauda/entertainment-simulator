package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.util.Date;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria}
 * {@link net.sf.mmm.util.transferobject.api.TransferObject TO} used to find
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.Customer}s.
 *
 */
public class CustomerSearchCriteriaTo extends SearchCriteriaTo {

	private static final long serialVersionUID = 1L;

	private String name;
	private Integer contactNumber;
	private Boolean isRegularCustomer;
	private Date registrationDate;

	/**
	 * The constructor.
	 */
	public CustomerSearchCriteriaTo() {

		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Integer contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Boolean getIsRegularCustomer() {
		return isRegularCustomer;
	}

	public void setIsRegularCustomer(Boolean isRegularCustomer) {
		this.isRegularCustomer = isRegularCustomer;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

}
