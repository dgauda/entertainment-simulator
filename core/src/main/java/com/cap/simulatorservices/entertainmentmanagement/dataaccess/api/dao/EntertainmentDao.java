package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Entertainment entities
 */
public interface EntertainmentDao extends ApplicationDao<EntertainmentEntity> {

	/**
	 * Finds the {@link EntertainmentEntity entertainments} matching the given
	 * {@link EntertainmentSearchCriteriaTo}.
	 *
	 * @param criteria
	 *            is the {@link EntertainmentSearchCriteriaTo}.
	 * @return the {@link PaginatedListTo} with the matching
	 *         {@link EntertainmentEntity} objects.
	 */
	PaginatedListTo<EntertainmentEntity> findEntertainments(EntertainmentSearchCriteriaTo criteria);
}
