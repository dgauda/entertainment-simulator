package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of ShowDetails
 */
public class ShowDetailsCto extends AbstractCto {

	private static final long serialVersionUID = 1L;

	private ShowDetailsEto showDetails;

	private EntertainmentEto entertainmentId;

	public ShowDetailsEto getShowDetails() {
		return showDetails;
	}

	public void setShowDetails(ShowDetailsEto showDetails) {
		this.showDetails = showDetails;
	}

	public EntertainmentEto getEntertainmentId() {
		return entertainmentId;
	}

	public void setEntertainmentId(EntertainmentEto entertainmentId) {
		this.entertainmentId = entertainmentId;
	}

}
