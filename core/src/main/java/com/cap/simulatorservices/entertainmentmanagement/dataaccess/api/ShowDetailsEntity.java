package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cap.simulatorservices.entertainmentmanagement.common.api.ShowDetails;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author lebhatia
 */
@Entity
@Table(name = "Entertainment_Showdetails")
public class ShowDetailsEntity extends ApplicationPersistenceEntity implements ShowDetails {

	private static final long serialVersionUID = 1L;

	private Timestamp showTime;

	private String classType;

	private EntertainmentEntity entertainmentId;

	public ShowDetailsEntity() {

		super();
	}

	public ShowDetailsEntity(Timestamp showTime, String classType, EntertainmentEntity entertainmentId) {

		super();
		this.showTime = showTime;
		this.classType = classType;
		this.entertainmentId = entertainmentId;
	}

	/**
	 * @return the showTime
	 */
	public Timestamp getShowTime() {
		return showTime;
	}

	/**
	 * @param showTime
	 *            the showTime to set
	 */
	public void setShowTime(Timestamp showTime) {
		this.showTime = showTime;
	}

	/**
	 * @return classType
	 */
	public String getClassType() {

		return this.classType;
	}

	/**
	 * @param classType
	 *            new value of {@link #getclassType}.
	 */
	public void setClassType(String classType) {

		this.classType = classType;
	}

	/**
	 * @return entertainmentId
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "entertainmentid")
	public EntertainmentEntity getEntertainmentId() {

		return this.entertainmentId;
	}

	/**
	 * @param entertainmentId
	 *            new value of {@link #getentertainmentId}.
	 */
	public void setEntertainmentId(EntertainmentEntity entertainmentId) {

		this.entertainmentId = entertainmentId;
	}

	@Override
	@Transient
	public Long getEntertainmentIdId() {

		if (this.entertainmentId == null) {
			return null;
		}
		return this.entertainmentId.getId();
	}

	@Override
	public void setEntertainmentIdId(Long entertainmentIdId) {

		if (entertainmentIdId == null) {
			this.entertainmentId = null;
		} else {
			EntertainmentEntity entertainmentEntity = new EntertainmentEntity();
			entertainmentEntity.setId(entertainmentIdId);
			this.entertainmentId = entertainmentEntity;
		}
	}

}
