package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Date;

import com.cap.simulatorservices.entertainmentmanagement.common.api.EntertainmentReservation;
import com.cap.simulatorservices.general.common.api.to.AbstractEto;

/**
 * Entity transport object of Reservation
 */
public class EntertainmentReservationEto extends AbstractEto implements EntertainmentReservation {

	private static final long serialVersionUID = 1L;

	private Date bookingDate;
	private Integer noOfTickets;

	private Long customeridId;

	private Long showidId;

	@Override
	public Date getBookingDate() {
		return bookingDate;
	}

	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	@Override
	public Integer getNoOfTickets() {
		return noOfTickets;
	}

	@Override
	public void setNoOfTickets(Integer noOfTickets) {
		this.noOfTickets = noOfTickets;
	}

	@Override
	public Long getCustomeridId() {
		return customeridId;
	}

	@Override
	public void setCustomeridId(Long customeridId) {
		this.customeridId = customeridId;
	}

	@Override
	public Long getShowidId() {
		return showidId;
	}

	@Override
	public void setShowidId(Long showidId) {
		this.showidId = showidId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.bookingDate == null) ? 0 : this.bookingDate.hashCode());
		result = prime * result + ((this.noOfTickets == null) ? 0 : this.noOfTickets.hashCode());

		result = prime * result + ((this.customeridId == null) ? 0 : this.customeridId.hashCode());

		result = prime * result + ((this.showidId == null) ? 0 : this.showidId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		// class check will be done by super type EntityTo!
		if (!super.equals(obj)) {
			return false;
		}
		EntertainmentReservationEto other = (EntertainmentReservationEto) obj;
		if (this.bookingDate == null) {
			if (other.bookingDate != null) {
				return false;
			}
		} else if (!this.bookingDate.equals(other.bookingDate)) {
			return false;
		}
		if (this.noOfTickets == null) {
			if (other.noOfTickets != null) {
				return false;
			}
		} else if (!this.noOfTickets.equals(other.noOfTickets)) {
			return false;
		}

		if (this.customeridId == null) {
			if (other.customeridId != null) {
				return false;
			}
		} else if (!this.customeridId.equals(other.customeridId)) {
			return false;
		}

		if (this.showidId == null) {
			if (other.showidId != null) {
				return false;
			}
		} else if (!this.showidId.equals(other.showidId)) {
			return false;
		}
		return true;
	}
}
