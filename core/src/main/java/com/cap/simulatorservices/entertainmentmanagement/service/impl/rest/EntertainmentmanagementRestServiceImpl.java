package com.cap.simulatorservices.entertainmentmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.Entertainmentmanagement;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.service.api.rest.EntertainmentmanagementRestService;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of
 * component {@link Entertainmentmanagement}.
 */
@Named("EntertainmentmanagementRestService")
public class EntertainmentmanagementRestServiceImpl implements EntertainmentmanagementRestService {

	@Inject
	private Entertainmentmanagement entertainmentmanagement;

	@Override
	public CustomerEto getCustomer(long id) {
		return this.entertainmentmanagement.findCustomer(id);
	}

	@Override
	public CustomerEto saveCustomer(CustomerEto customer) {
		return this.entertainmentmanagement.saveCustomer(customer);
	}

	@Override
	public void deleteCustomer(long id) {
		this.entertainmentmanagement.deleteCustomer(id);
	}

	@Override
	public PaginatedListTo<CustomerEto> findCustomersByPost(CustomerSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findCustomerEtos(searchCriteriaTo);
	}

	@Override
	public CustomerCto getCustomerCto(long id) {
		return this.entertainmentmanagement.findCustomerCto(id);
	}

	@Override
	public PaginatedListTo<CustomerCto> findCustomerCtosByPost(CustomerSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findCustomerCtos(searchCriteriaTo);
	}

	@Override
	public EntertainmentEto getEntertainment(long id) {
		return this.entertainmentmanagement.findEntertainment(id);
	}

	@Override
	public EntertainmentEto saveEntertainment(EntertainmentEto entertainment) {
		return this.entertainmentmanagement.saveEntertainment(entertainment);
	}

	@Override
	public void deleteEntertainment(long id) {
		this.entertainmentmanagement.deleteEntertainment(id);
	}

	@Override
	public PaginatedListTo<EntertainmentEto> findEntertainmentsByPost(EntertainmentSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findEntertainmentEtos(searchCriteriaTo);
	}

	@Override
	public EntertainmentCto getEntertainmentCto(long id) {
		return this.entertainmentmanagement.findEntertainmentCto(id);
	}

	@Override
	public PaginatedListTo<EntertainmentCto> findEntertainmentCtosByPost(
			EntertainmentSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findEntertainmentCtos(searchCriteriaTo);
	}

	@Override
	public EntertainmentReservationEto getReservation(long id) {
		return this.entertainmentmanagement.findReservation(id);
	}

	@Override
	public EntertainmentReservationEto saveReservation(EntertainmentReservationEto reservation) {
		return this.entertainmentmanagement.saveReservation(reservation);
	}

	@Override
	public void deleteReservation(long id) {
		this.entertainmentmanagement.deleteReservation(id);
	}

	@Override
	public PaginatedListTo<EntertainmentReservationEto> findReservationsByPost(EntertainmentReservationSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findReservationEtos(searchCriteriaTo);
	}

	@Override
	public EntertainmentReservationCto getReservationCto(long id) {
		return this.entertainmentmanagement.findReservationCto(id);
	}

	@Override
	public PaginatedListTo<EntertainmentReservationCto> findReservationCtosByPost(EntertainmentReservationSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findReservationCtos(searchCriteriaTo);
	}

	@Override
	public ShowDetailsEto getShowDetails(long id) {
		return this.entertainmentmanagement.findShowDetails(id);
	}

	@Override
	public ShowDetailsEto saveShowDetails(ShowDetailsEto showdetails) {
		return this.entertainmentmanagement.saveShowDetails(showdetails);
	}

	@Override
	public void deleteShowDetails(long id) {
		this.entertainmentmanagement.deleteShowDetails(id);
	}

	@Override
	public PaginatedListTo<ShowDetailsEto> findShowDetailssByPost(ShowDetailsSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findShowDetailsEtos(searchCriteriaTo);
	}

	@Override
	public ShowDetailsCto getShowDetailsCto(long id) {
		return this.entertainmentmanagement.findShowDetailsCto(id);
	}

	@Override
	public PaginatedListTo<ShowDetailsCto> findShowDetailsCtosByPost(ShowDetailsSearchCriteriaTo searchCriteriaTo) {
		return this.entertainmentmanagement.findShowDetailsCtos(searchCriteriaTo);
	}

}
