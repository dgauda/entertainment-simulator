package com.cap.simulatorservices.entertainmentmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.Entertainmentmanagement;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of
 * component {@link Entertainmentmanagement}.
 */
@Path("/entertainmentmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface EntertainmentmanagementRestService {

	/**
	 * Delegates to {@link Entertainmentmanagement#findCustomer}.
	 *
	 * @param id
	 *            the ID of the {@link CustomerEto}
	 * @return the {@link CustomerEto}
	 */
	@GET
	@Path("/customer/{id}/")
	public CustomerEto getCustomer(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#saveCustomer}.
	 *
	 * @param customer
	 *            the {@link CustomerEto} to be saved
	 * @return the recently created {@link CustomerEto}
	 */
	@POST
	@Path("/customer/")
	public CustomerEto saveCustomer(CustomerEto customer);

	/**
	 * Delegates to {@link Entertainmentmanagement#deleteCustomer}.
	 *
	 * @param id
	 *            ID of the {@link CustomerEto} to be deleted
	 */
	@DELETE
	@Path("/customer/{id}/")
	public void deleteCustomer(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findCustomerEtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            customers.
	 * @return the {@link PaginatedListTo list} of matching {@link CustomerEto}s.
	 */
	@Path("/customer/search")
	@POST
	public PaginatedListTo<CustomerEto> findCustomersByPost(CustomerSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findCustomerCto}.
	 *
	 * @param id
	 *            the ID of the {@link CustomerCto}
	 * @return the {@link CustomerCto}
	 */
	@GET
	@Path("/customer/cto/{id}/")
	public CustomerCto getCustomerCto(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findCustomerCtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            customers.
	 * @return the {@link PaginatedListTo list} of matching {@link CustomerCto}s.
	 */
	@Path("/customer/cto/search")
	@POST
	public PaginatedListTo<CustomerCto> findCustomerCtosByPost(CustomerSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findEntertainment}.
	 *
	 * @param id
	 *            the ID of the {@link EntertainmentEto}
	 * @return the {@link EntertainmentEto}
	 */
	@GET
	@Path("/entertainment/{id}/")
	public EntertainmentEto getEntertainment(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#saveEntertainment}.
	 *
	 * @param entertainment
	 *            the {@link EntertainmentEto} to be saved
	 * @return the recently created {@link EntertainmentEto}
	 */
	@POST
	@Path("/entertainment/")
	public EntertainmentEto saveEntertainment(EntertainmentEto entertainment);

	/**
	 * Delegates to {@link Entertainmentmanagement#deleteEntertainment}.
	 *
	 * @param id
	 *            ID of the {@link EntertainmentEto} to be deleted
	 */
	@DELETE
	@Path("/entertainment/{id}/")
	public void deleteEntertainment(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findEntertainmentEtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            entertainments.
	 * @return the {@link PaginatedListTo list} of matching
	 *         {@link EntertainmentEto}s.
	 */
	@Path("/entertainment/search")
	@POST
	public PaginatedListTo<EntertainmentEto> findEntertainmentsByPost(EntertainmentSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findEntertainmentCto}.
	 *
	 * @param id
	 *            the ID of the {@link EntertainmentCto}
	 * @return the {@link EntertainmentCto}
	 */
	@GET
	@Path("/entertainment/cto/{id}/")
	public EntertainmentCto getEntertainmentCto(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findEntertainmentCtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            entertainments.
	 * @return the {@link PaginatedListTo list} of matching
	 *         {@link EntertainmentCto}s.
	 */
	@Path("/entertainment/cto/search")
	@POST
	public PaginatedListTo<EntertainmentCto> findEntertainmentCtosByPost(
			EntertainmentSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findReservation}.
	 *
	 * @param id
	 *            the ID of the {@link EntertainmentReservationEto}
	 * @return the {@link EntertainmentReservationEto}
	 */
	@GET
	@Path("/reservation/{id}/")
	public EntertainmentReservationEto getReservation(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#saveReservation}.
	 *
	 * @param reservation
	 *            the {@link EntertainmentReservationEto} to be saved
	 * @return the recently created {@link EntertainmentReservationEto}
	 */
	@POST
	@Path("/reservation/")
	public EntertainmentReservationEto saveReservation(EntertainmentReservationEto reservation);

	/**
	 * Delegates to {@link Entertainmentmanagement#deleteReservation}.
	 *
	 * @param id
	 *            ID of the {@link EntertainmentReservationEto} to be deleted
	 */
	@DELETE
	@Path("/reservation/{id}/")
	public void deleteReservation(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findReservationEtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            reservations.
	 * @return the {@link PaginatedListTo list} of matching {@link EntertainmentReservationEto}s.
	 */
	@Path("/reservation/search")
	@POST
	public PaginatedListTo<EntertainmentReservationEto> findReservationsByPost(EntertainmentReservationSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findReservationCto}.
	 *
	 * @param id
	 *            the ID of the {@link EntertainmentReservationCto}
	 * @return the {@link EntertainmentReservationCto}
	 */
	@GET
	@Path("/reservation/cto/{id}/")
	public EntertainmentReservationCto getReservationCto(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findReservationCtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            reservations.
	 * @return the {@link PaginatedListTo list} of matching {@link EntertainmentReservationCto}s.
	 */
	@Path("/reservation/cto/search")
	@POST
	public PaginatedListTo<EntertainmentReservationCto> findReservationCtosByPost(EntertainmentReservationSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findShowDetails}.
	 *
	 * @param id
	 *            the ID of the {@link ShowDetailsEto}
	 * @return the {@link ShowDetailsEto}
	 */
	@GET
	@Path("/showdetails/{id}/")
	public ShowDetailsEto getShowDetails(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#saveShowDetails}.
	 *
	 * @param showdetails
	 *            the {@link ShowDetailsEto} to be saved
	 * @return the recently created {@link ShowDetailsEto}
	 */
	@POST
	@Path("/showdetails/")
	public ShowDetailsEto saveShowDetails(ShowDetailsEto showdetails);

	/**
	 * Delegates to {@link Entertainmentmanagement#deleteShowDetails}.
	 *
	 * @param id
	 *            ID of the {@link ShowDetailsEto} to be deleted
	 */
	@DELETE
	@Path("/showdetails/{id}/")
	public void deleteShowDetails(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findShowDetailsEtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            showdetailss.
	 * @return the {@link PaginatedListTo list} of matching {@link ShowDetailsEto}s.
	 */
	@Path("/showdetails/search")
	@POST
	public PaginatedListTo<ShowDetailsEto> findShowDetailssByPost(ShowDetailsSearchCriteriaTo searchCriteriaTo);

	/**
	 * Delegates to {@link Entertainmentmanagement#findShowDetailsCto}.
	 *
	 * @param id
	 *            the ID of the {@link ShowDetailsCto}
	 * @return the {@link ShowDetailsCto}
	 */
	@GET
	@Path("/showdetails/cto/{id}/")
	public ShowDetailsCto getShowDetailsCto(@PathParam("id") long id);

	/**
	 * Delegates to {@link Entertainmentmanagement#findShowDetailsCtos}.
	 *
	 * @param searchCriteriaTo
	 *            the pagination and search criteria to be used for finding
	 *            showdetailss.
	 * @return the {@link PaginatedListTo list} of matching {@link ShowDetailsCto}s.
	 */
	@Path("/showdetails/cto/search")
	@POST
	public PaginatedListTo<ShowDetailsCto> findShowDetailsCtosByPost(ShowDetailsSearchCriteriaTo searchCriteriaTo);

}
