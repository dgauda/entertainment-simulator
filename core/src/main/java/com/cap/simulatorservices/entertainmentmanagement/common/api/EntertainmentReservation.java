package com.cap.simulatorservices.entertainmentmanagement.common.api;

import java.sql.Date;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface EntertainmentReservation extends ApplicationEntity {

	public Date getBookingDate();

	public void setBookingDate(Date bookingDate);

	public Integer getNoOfTickets();

	public void setNoOfTickets(Integer noOfTickets);

	public Long getCustomeridId();

	public void setCustomeridId(Long customeridId);

	public Long getShowidId();

	public void setShowidId(Long showidId);

}
