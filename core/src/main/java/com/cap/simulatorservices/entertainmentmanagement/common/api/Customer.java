package com.cap.simulatorservices.entertainmentmanagement.common.api;

import java.util.Date;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface Customer extends ApplicationEntity {

	public String getName();

	public void setName(String name);

	public Integer getContactNumber();

	public void setContactNumber(Integer contactNumber);

	public Boolean getIsRegularCustomer();

	public void setIsRegularCustomer(Boolean isRegularCustomer);

	public Date getRegistrationDate();

	public void setRegistrationDate(Date registrationDate);

}
