package com.cap.simulatorservices.entertainmentmanagement.dataaccess.impl.dao;


import java.sql.Date;
import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentReservationEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.EntertainmentReservationDao;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link EntertainmentReservationDao}.
 */
@Named
public class EntertainmentReservationDaoImpl extends ApplicationDaoImpl<EntertainmentReservationEntity> implements EntertainmentReservationDao {

	/**
	 * The constructor.
	 */
	public EntertainmentReservationDaoImpl() {

		super();
	}

	@Override
	public Class<EntertainmentReservationEntity> getEntityClass() {
		return EntertainmentReservationEntity.class;
	}

	@Override
	public PaginatedListTo<EntertainmentReservationEntity> findReservations(EntertainmentReservationSearchCriteriaTo criteria) {

		EntertainmentReservationEntity reservation = Alias.alias(EntertainmentReservationEntity.class);
		EntityPathBase<EntertainmentReservationEntity> alias = Alias.$(reservation);
		JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

		Date bookingDate = criteria.getBookingDate();
		if (bookingDate != null) {
			query.where(Alias.$(reservation.getBookingDate()).eq(bookingDate));
		}
		Integer noOfTickets = criteria.getNoOfTickets();
		if (noOfTickets != null) {
			query.where(Alias.$(reservation.getNoOfTickets()).eq(noOfTickets));
		}
		Long customerid = criteria.getCustomeridId();
		if (customerid != null) {
			if (reservation.getCustomerid() != null) {
				query.where(Alias.$(reservation.getCustomerid().getId()).eq(customerid));
			}
		}
		Long showid = criteria.getShowidId();
		if (showid != null) {
			if (reservation.getShowid() != null) {
				query.where(Alias.$(reservation.getShowid().getId()).eq(showid));
			}
		}
		addOrderBy(query, alias, reservation, criteria.getSort());

		return findPaginated(criteria, query, alias);
	}

	private void addOrderBy(JPAQuery query, EntityPathBase<EntertainmentReservationEntity> alias, EntertainmentReservationEntity reservation,
			List<OrderByTo> sort) {
		if (sort != null && !sort.isEmpty()) {
			for (OrderByTo orderEntry : sort) {
				switch (orderEntry.getName()) {
				case "bookingDate":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(reservation.getBookingDate()).asc());
					} else {
						query.orderBy(Alias.$(reservation.getBookingDate()).desc());
					}
					break;
				case "noOfTickets":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(reservation.getNoOfTickets()).asc());
					} else {
						query.orderBy(Alias.$(reservation.getNoOfTickets()).desc());
					}
					break;
				case "customerid":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(reservation.getCustomerid().getId()).asc());
					} else {
						query.orderBy(Alias.$(reservation.getCustomerid().getId()).desc());
					}
					break;
				case "showid":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(reservation.getShowid().getId()).asc());
					} else {
						query.orderBy(Alias.$(reservation.getShowid().getId()).desc());
					}
					break;
				}
			}
		}
	}

}