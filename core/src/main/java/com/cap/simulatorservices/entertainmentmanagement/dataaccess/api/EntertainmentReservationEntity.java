package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api;


import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.cap.simulatorservices.entertainmentmanagement.common.api.EntertainmentReservation;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author lebhatia
 */
@Entity
@Table(name = "Entertainment_Reservation")
public class EntertainmentReservationEntity extends ApplicationPersistenceEntity implements EntertainmentReservation {

	private static final long serialVersionUID = 1L;

	private Date bookingDate;

	private Integer noOfTickets;

	private CustomerEntity customerid;

	private ShowDetailsEntity showid;

	public EntertainmentReservationEntity() {

		super();
	}

	public EntertainmentReservationEntity(Date bookingDate, Integer noOfTickets, @NotNull CustomerEntity customerid,
			@NotNull ShowDetailsEntity showid) {

		super();
		this.bookingDate = bookingDate;
		this.noOfTickets = noOfTickets;
		this.customerid = customerid;
		this.showid = showid;
	}

	/**
	 * @return bookingDate
	 */
	public Date getBookingDate() {

		return this.bookingDate;
	}

	/**
	 * @param bookingDate
	 *            new value of {@link #getbookingDate}.
	 */
	public void setBookingDate(Date bookingDate) {

		this.bookingDate = bookingDate;
	}

	/**
	 * @return noOfTickets
	 */
	public Integer getNoOfTickets() {

		return this.noOfTickets;
	}

	/**
	 * @param noOfTickets
	 *            new value of {@link #getnoOfTickets}.
	 */
	public void setNoOfTickets(Integer noOfTickets) {

		this.noOfTickets = noOfTickets;
	}

	/**
	 * @return customerId
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "customerid")
	public CustomerEntity getCustomerid() {

		return this.customerid;
	}

	/**
	 * @param customerId
	 *            new value of {@link #getcustomerId}.
	 */
	public void setCustomerid(CustomerEntity customerid) {

		this.customerid = customerid;
	}

	/**
	 * @return the showId
	 */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "showid")
	public ShowDetailsEntity getShowid() {
		return showid;
	}

	/**
	 * @param showId
	 *            the showId to set
	 */
	public void setShowid(ShowDetailsEntity showid) {
		this.showid = showid;
	}

	@Override
	@Transient
	public Long getCustomeridId() {

		if (this.customerid == null) {
			return null;
		}
		return this.customerid.getId();
	}

	@Override
	public void setCustomeridId(Long customeridId) {

		if (customeridId == null) {
			this.customerid = null;
		} else {
			CustomerEntity customerEntity = new CustomerEntity();
			customerEntity.setId(customeridId);
			this.customerid = customerEntity;
		}
	}

	@Override
	@Transient
	public Long getShowidId() {

		if (this.showid == null) {
			return null;
		}
		return this.showid.getId();
	}

	@Override
	public void setShowidId(Long showidId) {

		if (showidId == null) {
			this.showid = null;
		} else {
			ShowDetailsEntity showDetailsEntity = new ShowDetailsEntity();
			showDetailsEntity.setId(showidId);
			this.showid = showDetailsEntity;
		}
	}

}
