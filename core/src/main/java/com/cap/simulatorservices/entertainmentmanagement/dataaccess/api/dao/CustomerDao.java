package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.CustomerEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Customer entities
 */
public interface CustomerDao extends ApplicationDao<CustomerEntity> {

	/**
	 * Finds the {@link CustomerEntity customers} matching the given
	 * {@link CustomerSearchCriteriaTo}.
	 *
	 * @param criteria
	 *            is the {@link CustomerSearchCriteriaTo}.
	 * @return the {@link PaginatedListTo} with the matching {@link CustomerEntity}
	 *         objects.
	 */
	PaginatedListTo<CustomerEntity> findCustomers(CustomerSearchCriteriaTo criteria);
}
