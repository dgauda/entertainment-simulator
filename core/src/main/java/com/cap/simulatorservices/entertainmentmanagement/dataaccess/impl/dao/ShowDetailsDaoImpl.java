package com.cap.simulatorservices.entertainmentmanagement.dataaccess.impl.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.ShowDetailsEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.ShowDetailsDao;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ShowDetailsDao}.
 */
@Named
public class ShowDetailsDaoImpl extends ApplicationDaoImpl<ShowDetailsEntity> implements ShowDetailsDao {

	/**
	 * The constructor.
	 */
	public ShowDetailsDaoImpl() {

		super();
	}

	@Override
	public Class<ShowDetailsEntity> getEntityClass() {
		return ShowDetailsEntity.class;
	}

	@Override
	public PaginatedListTo<ShowDetailsEntity> findShowDetailss(ShowDetailsSearchCriteriaTo criteria) {

		ShowDetailsEntity showdetails = Alias.alias(ShowDetailsEntity.class);
		EntityPathBase<ShowDetailsEntity> alias = Alias.$(showdetails);
		JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

		Timestamp showTime = criteria.getShowTime();
		if (showTime != null) {
			query.where(Alias.$(showdetails.getShowTime()).eq(showTime));
		}
		String classType = criteria.getClassType();
		if (classType != null) {
			query.where(Alias.$(showdetails.getClassType()).eq(classType));
		}
		Long entertainmentId = criteria.getEntertainmentIdId();
		if (entertainmentId != null) {
			if (showdetails.getEntertainmentId() != null) {
				query.where(Alias.$(showdetails.getEntertainmentId().getId()).eq(entertainmentId));
			}
		}
		addOrderBy(query, alias, showdetails, criteria.getSort());

		return findPaginated(criteria, query, alias);
	}

	private void addOrderBy(JPAQuery query, EntityPathBase<ShowDetailsEntity> alias, ShowDetailsEntity showdetails,
			List<OrderByTo> sort) {
		if (sort != null && !sort.isEmpty()) {
			for (OrderByTo orderEntry : sort) {
				switch (orderEntry.getName()) {
				case "showTime":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(showdetails.getShowTime()).asc());
					} else {
						query.orderBy(Alias.$(showdetails.getShowTime()).desc());
					}
					break;
				case "classType":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(showdetails.getClassType()).asc());
					} else {
						query.orderBy(Alias.$(showdetails.getClassType()).desc());
					}
					break;
				case "entertainmentId":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(showdetails.getEntertainmentId().getId()).asc());
					} else {
						query.orderBy(Alias.$(showdetails.getEntertainmentId().getId()).desc());
					}
					break;
				}
			}
		}
	}

}