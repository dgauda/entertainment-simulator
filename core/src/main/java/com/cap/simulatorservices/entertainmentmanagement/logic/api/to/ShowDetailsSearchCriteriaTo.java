package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Timestamp;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria}
 * {@link net.sf.mmm.util.transferobject.api.TransferObject TO} used to find
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.ShowDetails}s.
 *
 */
public class ShowDetailsSearchCriteriaTo extends SearchCriteriaTo {

	private static final long serialVersionUID = 1L;

	private Timestamp showTime;
	private String classType;

	private Long entertainmentIdId;

	/**
	 * The constructor.
	 */
	public ShowDetailsSearchCriteriaTo() {

		super();
	}

	public Timestamp getShowTime() {
		return showTime;
	}

	public void setShowTime(Timestamp showTime) {
		this.showTime = showTime;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public Long getEntertainmentIdId() {
		return entertainmentIdId;
	}

	public void setEntertainmentIdId(Long entertainmentIdId) {
		this.entertainmentIdId = entertainmentIdId;
	}

}
