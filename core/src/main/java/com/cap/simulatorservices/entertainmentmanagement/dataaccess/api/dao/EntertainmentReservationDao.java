package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentReservationEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Reservation entities
 */
public interface EntertainmentReservationDao extends ApplicationDao<EntertainmentReservationEntity> {

	/**
	 * Finds the {@link EntertainmentReservationEntity reservations} matching the given
	 * {@link EntertainmentReservationSearchCriteriaTo}.
	 *
	 * @param criteria
	 *            is the {@link EntertainmentReservationSearchCriteriaTo}.
	 * @return the {@link PaginatedListTo} with the matching
	 *         {@link EntertainmentReservationEntity} objects.
	 */
	PaginatedListTo<EntertainmentReservationEntity> findReservations(EntertainmentReservationSearchCriteriaTo criteria);
}
