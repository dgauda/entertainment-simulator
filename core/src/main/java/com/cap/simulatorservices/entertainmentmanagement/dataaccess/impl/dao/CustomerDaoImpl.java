package com.cap.simulatorservices.entertainmentmanagement.dataaccess.impl.dao;

import java.util.Date;
import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.CustomerEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.CustomerDao;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link CustomerDao}.
 */
@Named
public class CustomerDaoImpl extends ApplicationDaoImpl<CustomerEntity> implements CustomerDao {

	/**
	 * The constructor.
	 */
	public CustomerDaoImpl() {

		super();
	}

	@Override
	public Class<CustomerEntity> getEntityClass() {
		return CustomerEntity.class;
	}

	@Override
	public PaginatedListTo<CustomerEntity> findCustomers(CustomerSearchCriteriaTo criteria) {

		CustomerEntity customer = Alias.alias(CustomerEntity.class);
		EntityPathBase<CustomerEntity> alias = Alias.$(customer);
		JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

		String name = criteria.getName();
		if (name != null) {
			query.where(Alias.$(customer.getName()).eq(name));
		}
		Integer contactNumber = criteria.getContactNumber();
		if (contactNumber != null) {
			query.where(Alias.$(customer.getContactNumber()).eq(contactNumber));
		}
		Boolean isRegularCustomer = criteria.getIsRegularCustomer();
		if (isRegularCustomer != null) {
			query.where(Alias.$(customer.getIsRegularCustomer()).eq(isRegularCustomer));
		}
		Date registrationDate = criteria.getRegistrationDate();
		if (registrationDate != null) {
			query.where(Alias.$(customer.getRegistrationDate()).eq(registrationDate));
		}
		addOrderBy(query, alias, customer, criteria.getSort());

		return findPaginated(criteria, query, alias);
	}

	private void addOrderBy(JPAQuery query, EntityPathBase<CustomerEntity> alias, CustomerEntity customer,
			List<OrderByTo> sort) {
		if (sort != null && !sort.isEmpty()) {
			for (OrderByTo orderEntry : sort) {
				switch (orderEntry.getName()) {
				case "name":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(customer.getName()).asc());
					} else {
						query.orderBy(Alias.$(customer.getName()).desc());
					}
					break;
				case "contactNumber":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(customer.getContactNumber()).asc());
					} else {
						query.orderBy(Alias.$(customer.getContactNumber()).desc());
					}
					break;
				case "isRegularCustomer":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(customer.getIsRegularCustomer()).asc());
					} else {
						query.orderBy(Alias.$(customer.getIsRegularCustomer()).desc());
					}
					break;
				case "registrationDate":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(customer.getRegistrationDate()).asc());
					} else {
						query.orderBy(Alias.$(customer.getRegistrationDate()).desc());
					}
					break;
				}
			}
		}
	}

}