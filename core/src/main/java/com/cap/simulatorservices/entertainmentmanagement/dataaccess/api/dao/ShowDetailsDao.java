package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.ShowDetailsEntity;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ShowDetails entities
 */
public interface ShowDetailsDao extends ApplicationDao<ShowDetailsEntity> {

	/**
	 * Finds the {@link ShowDetailsEntity showdetailss} matching the given
	 * {@link ShowDetailsSearchCriteriaTo}.
	 *
	 * @param criteria
	 *            is the {@link ShowDetailsSearchCriteriaTo}.
	 * @return the {@link PaginatedListTo} with the matching
	 *         {@link ShowDetailsEntity} objects.
	 */
	PaginatedListTo<ShowDetailsEntity> findShowDetailss(ShowDetailsSearchCriteriaTo criteria);
}
