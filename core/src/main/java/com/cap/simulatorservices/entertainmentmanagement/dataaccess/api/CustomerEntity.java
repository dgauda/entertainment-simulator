package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.cap.simulatorservices.entertainmentmanagement.common.api.Customer;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author lebhatia
 */
@Entity
@Table(name = "Entertainment_Customer")
public class CustomerEntity extends ApplicationPersistenceEntity implements Customer {

	private String name;

	private Integer contactNumber;

	private Boolean isRegularCustomer;

	private Date registrationDate;

	private static final long serialVersionUID = 1L;

	public CustomerEntity() {

		super();
	}

	public CustomerEntity(String name, Integer contactNumber, Boolean isRegularCustomer,
			Date registrationDate) {
		super();
		this.name = name;
		this.contactNumber = contactNumber;
		this.isRegularCustomer = isRegularCustomer;
		this.registrationDate = registrationDate;
	}

	/**
	 * @return name
	 */
	public String getName() {

		return this.name;
	}

	/**
	 * @param name
	 *            new value of {@link #getname}.
	 */
	public void setName(String name) {

		this.name = name;
	}

	/**
	 * @return contactNumber
	 */
	public Integer getContactNumber() {

		return this.contactNumber;
	}

	/**
	 * @param contactNumber
	 *            new value of {@link #getcontactNumber}.
	 */
	public void setContactNumber(Integer contactNumber) {

		this.contactNumber = contactNumber;
	}

	/**
	 * @return isRegularCustomer
	 */
	public Boolean getIsRegularCustomer() {

		return this.isRegularCustomer;
	}

	/**
	 * @param isRegularCustomer
	 *            new value of {@link #getisRegularCustomer}.
	 */
	public void setIsRegularCustomer(Boolean isRegularCustomer) {

		this.isRegularCustomer = isRegularCustomer;
	}

	/**
	 * @return registrationDate
	 */
	public Date getRegistrationDate() {

		return this.registrationDate;
	}

	/**
	 * @param registrationDate
	 *            new value of {@link #getregistrationDate}.
	 */
	public void setRegistrationDate(Date registrationDate) {

		this.registrationDate = registrationDate;
	}

	@Override
	public String toString() {
		return "CustomerEntity [name=" + name + ", contactNumber=" + contactNumber + ", isRegularCustomer="
				+ isRegularCustomer + ", registrationDate=" + registrationDate + "]";
	}

}
