package com.cap.simulatorservices.entertainmentmanagement.dataaccess.api;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.cap.simulatorservices.entertainmentmanagement.common.api.Entertainment;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author lebhatia
 */
@Entity
@Table(name = "Entertainment")
public class EntertainmentEntity extends ApplicationPersistenceEntity implements Entertainment {

	private String address;

	private Integer zip;

	private String entertainmentType;

	private static final long serialVersionUID = 1L;

	public EntertainmentEntity() {

		super();
	}

	public EntertainmentEntity(String address, Integer zip, String entertainmentType) {

		super();
		this.address = address;
		this.zip = zip;
		this.entertainmentType = entertainmentType;
	}

	/**
	 * @return address
	 */
	public String getAddress() {

		return this.address;
	}

	/**
	 * @param address
	 *            new value of {@link #getaddress}.
	 */
	public void setAddress(String address) {

		this.address = address;
	}

	/**
	 * @return zip
	 */
	public Integer getZip() {

		return this.zip;
	}

	/**
	 * @param zip
	 *            new value of {@link #getzip}.
	 */
	public void setZip(Integer zip) {

		this.zip = zip;
	}

	/**
	 * @return entertainmentType
	 */
	public String getEntertainmentType() {

		return this.entertainmentType;
	}

	/**
	 * @param entertainmentType
	 *            new value of {@link #getentertainmentType}.
	 */
	public void setEntertainmentType(String entertainmentType) {

		this.entertainmentType = entertainmentType;
	}

	@Override
	public String toString() {
		return "EntertainmentEntity [address=" + address + ", zip=" + zip + ", entertainmentType=" + entertainmentType
				+ "]";
	}

}
