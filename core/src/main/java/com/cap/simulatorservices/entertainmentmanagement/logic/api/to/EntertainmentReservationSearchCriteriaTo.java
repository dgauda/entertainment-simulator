package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.sql.Date;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria}
 * {@link net.sf.mmm.util.transferobject.api.TransferObject TO} used to find
 * {@link com.cap.simulatorservices.entertainmentmanagement.common.api.EntertainmentReservation}s.
 *
 */
public class EntertainmentReservationSearchCriteriaTo extends SearchCriteriaTo {

	private static final long serialVersionUID = 1L;

	private Date bookingDate;
	private Integer noOfTickets;

	private Long customeridId;

	private Long showidId;

	/**
	 * The constructor.
	 */
	public EntertainmentReservationSearchCriteriaTo() {

		super();
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Integer getNoOfTickets() {
		return noOfTickets;
	}

	public void setNoOfTickets(Integer noOfTickets) {
		this.noOfTickets = noOfTickets;
	}

	public Long getCustomeridId() {
		return customeridId;
	}

	public void setCustomeridId(Long customeridId) {
		this.customeridId = customeridId;
	}

	public Long getShowidId() {
		return showidId;
	}

	public void setShowidId(Long showidId) {
		this.showidId = showidId;
	}

}
