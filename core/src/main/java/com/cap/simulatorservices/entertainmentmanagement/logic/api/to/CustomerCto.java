package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Customer
 */
public class CustomerCto extends AbstractCto {

	private static final long serialVersionUID = 1L;

	private CustomerEto customer;

	public CustomerEto getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEto customer) {
		this.customer = customer;
	}

}
