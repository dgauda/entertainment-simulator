package com.cap.simulatorservices.entertainmentmanagement.logic.api;

import java.util.List;

import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Interface for Entertainmentmanagement component.
 */
public interface Entertainmentmanagement {

	/**
	 * Returns a Customer by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the Customer.
	 * @return The {@link CustomerEto} with id 'id'
	 */
	CustomerEto findCustomer(Long id);

	/**
	 * Returns a paginated list of Customers matching the search criteria.
	 *
	 * @param criteria
	 *            the {@link CustomerSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link CustomerEto}s.
	 */
	PaginatedListTo<CustomerEto> findCustomerEtos(CustomerSearchCriteriaTo criteria);

	/**
	 * Deletes a customer from the database by its id 'customerId'.
	 *
	 * @param customerId
	 *            Id of the customer to delete
	 * @return boolean <code>true</code> if the customer can be deleted,
	 *         <code>false</code> otherwise
	 */
	boolean deleteCustomer(Long customerId);

	/**
	 * Saves a customer and store it in the database.
	 *
	 * @param customer
	 *            the {@link CustomerEto} to create.
	 * @return the new {@link CustomerEto} that has been saved with ID and version.
	 */
	CustomerEto saveCustomer(CustomerEto customer);

	/**
	 * Returns a composite Customer by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the Customer.
	 * @return The {@link CustomerCto} with id 'id'
	 */
	CustomerCto findCustomerCto(Long id);

	/**
	 * Returns a paginated list of composite Customers matching the search criteria.
	 *
	 * @param criteria
	 *            the {@link CustomerSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link CustomerCto}s.
	 */
	PaginatedListTo<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo criteria);

	/**
	 * Returns a Entertainment by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the Entertainment.
	 * @return The {@link EntertainmentEto} with id 'id'
	 */
	EntertainmentEto findEntertainment(Long id);

	/**
	 * Returns a paginated list of Entertainments matching the search criteria.
	 *
	 * @param criteria
	 *            the {@link EntertainmentSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link EntertainmentEto}s.
	 */
	PaginatedListTo<EntertainmentEto> findEntertainmentEtos(EntertainmentSearchCriteriaTo criteria);

	/**
	 * Deletes a entertainment from the database by its id 'entertainmentId'.
	 *
	 * @param entertainmentId
	 *            Id of the entertainment to delete
	 * @return boolean <code>true</code> if the entertainment can be deleted,
	 *         <code>false</code> otherwise
	 */
	boolean deleteEntertainment(Long entertainmentId);

	/**
	 * Saves a entertainment and store it in the database.
	 *
	 * @param entertainment
	 *            the {@link EntertainmentEto} to create.
	 * @return the new {@link EntertainmentEto} that has been saved with ID and
	 *         version.
	 */
	EntertainmentEto saveEntertainment(EntertainmentEto entertainment);

	/**
	 * Returns a composite Entertainment by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the Entertainment.
	 * @return The {@link EntertainmentCto} with id 'id'
	 */
	EntertainmentCto findEntertainmentCto(Long id);

	/**
	 * Returns a paginated list of composite Entertainments matching the search
	 * criteria.
	 *
	 * @param criteria
	 *            the {@link EntertainmentSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link EntertainmentCto}s.
	 */
	PaginatedListTo<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo criteria);

	/**
	 * Returns a Reservation by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the Reservation.
	 * @return The {@link EntertainmentReservationEto} with id 'id'
	 */
	EntertainmentReservationEto findReservation(Long id);

	/**
	 * Returns a paginated list of Reservations matching the search criteria.
	 *
	 * @param criteria
	 *            the {@link EntertainmentReservationSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link EntertainmentReservationEto}s.
	 */
	PaginatedListTo<EntertainmentReservationEto> findReservationEtos(EntertainmentReservationSearchCriteriaTo criteria);

	/**
	 * Deletes a reservation from the database by its id 'reservationId'.
	 *
	 * @param reservationId
	 *            Id of the reservation to delete
	 * @return boolean <code>true</code> if the reservation can be deleted,
	 *         <code>false</code> otherwise
	 */
	boolean deleteReservation(Long reservationId);

	/**
	 * Saves a reservation and store it in the database.
	 *
	 * @param reservation
	 *            the {@link EntertainmentReservationEto} to create.
	 * @return the new {@link EntertainmentReservationEto} that has been saved with ID and
	 *         version.
	 */
	EntertainmentReservationEto saveReservation(EntertainmentReservationEto reservation);

	/**
	 * Returns a composite Reservation by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the Reservation.
	 * @return The {@link EntertainmentReservationCto} with id 'id'
	 */
	EntertainmentReservationCto findReservationCto(Long id);

	/**
	 * Returns a paginated list of composite Reservations matching the search
	 * criteria.
	 *
	 * @param criteria
	 *            the {@link EntertainmentReservationSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link EntertainmentReservationCto}s.
	 */
	PaginatedListTo<EntertainmentReservationCto> findReservationCtos(EntertainmentReservationSearchCriteriaTo criteria);

	/**
	 * Returns a ShowDetails by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the ShowDetails.
	 * @return The {@link ShowDetailsEto} with id 'id'
	 */
	ShowDetailsEto findShowDetails(Long id);

	/**
	 * Returns a paginated list of ShowDetailss matching the search criteria.
	 *
	 * @param criteria
	 *            the {@link ShowDetailsSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link ShowDetailsEto}s.
	 */
	PaginatedListTo<ShowDetailsEto> findShowDetailsEtos(ShowDetailsSearchCriteriaTo criteria);

	/**
	 * Deletes a showDetails from the database by its id 'showDetailsId'.
	 *
	 * @param showDetailsId
	 *            Id of the showDetails to delete
	 * @return boolean <code>true</code> if the showDetails can be deleted,
	 *         <code>false</code> otherwise
	 */
	boolean deleteShowDetails(Long showDetailsId);

	/**
	 * Saves a showDetails and store it in the database.
	 *
	 * @param showDetails
	 *            the {@link ShowDetailsEto} to create.
	 * @return the new {@link ShowDetailsEto} that has been saved with ID and
	 *         version.
	 */
	ShowDetailsEto saveShowDetails(ShowDetailsEto showDetails);

	/**
	 * Returns a composite ShowDetails by its id 'id'.
	 *
	 * @param id
	 *            The id 'id' of the ShowDetails.
	 * @return The {@link ShowDetailsCto} with id 'id'
	 */
	ShowDetailsCto findShowDetailsCto(Long id);

	/**
	 * Returns a paginated list of composite ShowDetailss matching the search
	 * criteria.
	 *
	 * @param criteria
	 *            the {@link ShowDetailsSearchCriteriaTo}.
	 * @return the {@link List} of matching {@link ShowDetailsCto}s.
	 */
	PaginatedListTo<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo criteria);

}
