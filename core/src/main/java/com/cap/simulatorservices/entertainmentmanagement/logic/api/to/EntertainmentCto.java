package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Entertainment
 */
public class EntertainmentCto extends AbstractCto {

	private static final long serialVersionUID = 1L;

	private EntertainmentEto entertainment;

	public EntertainmentEto getEntertainment() {
		return entertainment;
	}

	public void setEntertainment(EntertainmentEto entertainment) {
		this.entertainment = entertainment;
	}

}
