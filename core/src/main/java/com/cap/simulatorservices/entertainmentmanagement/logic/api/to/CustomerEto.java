package com.cap.simulatorservices.entertainmentmanagement.logic.api.to;

import java.util.Date;

import com.cap.simulatorservices.entertainmentmanagement.common.api.Customer;
import com.cap.simulatorservices.general.common.api.to.AbstractEto;

/**
 * Entity transport object of Customer
 */
public class CustomerEto extends AbstractEto implements Customer {

	private static final long serialVersionUID = 1L;

	private String name;
	private Integer contactNumber;
	private Boolean isRegularCustomer;
	private Date registrationDate;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Integer getContactNumber() {
		return contactNumber;
	}

	@Override
	public void setContactNumber(Integer contactNumber) {
		this.contactNumber = contactNumber;
	}

	@Override
	public Boolean getIsRegularCustomer() {
		return isRegularCustomer;
	}

	@Override
	public void setIsRegularCustomer(Boolean isRegularCustomer) {
		this.isRegularCustomer = isRegularCustomer;
	}

	@Override
	public Date getRegistrationDate() {
		return registrationDate;
	}

	@Override
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result + ((this.contactNumber == null) ? 0 : this.contactNumber.hashCode());
		result = prime * result + ((this.isRegularCustomer == null) ? 0 : this.isRegularCustomer.hashCode());
		result = prime * result + ((this.registrationDate == null) ? 0 : this.registrationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		// class check will be done by super type EntityTo!
		if (!super.equals(obj)) {
			return false;
		}
		CustomerEto other = (CustomerEto) obj;
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.contactNumber == null) {
			if (other.contactNumber != null) {
				return false;
			}
		} else if (!this.contactNumber.equals(other.contactNumber)) {
			return false;
		}
		if (this.isRegularCustomer == null) {
			if (other.isRegularCustomer != null) {
				return false;
			}
		} else if (!this.isRegularCustomer.equals(other.isRegularCustomer)) {
			return false;
		}
		if (this.registrationDate == null) {
			if (other.registrationDate != null) {
				return false;
			}
		} else if (!this.registrationDate.equals(other.registrationDate)) {
			return false;
		}
		return true;
	}
}
