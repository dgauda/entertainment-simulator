package com.cap.simulatorservices.entertainmentmanagement.logic.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.CustomerEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentReservationEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.ShowDetailsEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.CustomerDao;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.EntertainmentDao;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.EntertainmentReservationDao;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.ShowDetailsDao;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.Entertainmentmanagement;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.CustomerSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentReservationSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsCto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsEto;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.ShowDetailsSearchCriteriaTo;
import com.cap.simulatorservices.general.logic.base.AbstractComponentFacade;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;
import io.oasp.module.jpa.common.api.to.PaginationResultTo;

/**
 * Implementation of component interface of entertainmentmanagement
 */
@Named
@Transactional
public class EntertainmentmanagementImpl extends AbstractComponentFacade implements Entertainmentmanagement {

	/**
	 * Logger instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EntertainmentmanagementImpl.class);

	/**
	 * @see #getCustomerDao()
	 */
	@Inject
	private CustomerDao customerDao;

	/**
	 * @see #getEntertainmentDao()
	 */
	@Inject
	private EntertainmentDao entertainmentDao;

	/**
	 * @see #getReservationDao()
	 */
	@Inject
	private EntertainmentReservationDao reservationDao;

	/**
	 * @see #getShowDetailsDao()
	 */
	@Inject
	private ShowDetailsDao showDetailsDao;

	/**
	 * The constructor.
	 */
	public EntertainmentmanagementImpl() {

		super();
	}

	@Override
	public CustomerEto findCustomer(Long id) {
		LOG.debug("Get Customer with id {} from database.", id);
		return getBeanMapper().map(getCustomerDao().findOne(id), CustomerEto.class);
	}

	@Override
	public PaginatedListTo<CustomerEto> findCustomerEtos(CustomerSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<CustomerEntity> customers = getCustomerDao().findCustomers(criteria);
		return mapPaginatedEntityList(customers, CustomerEto.class);
	}

	@Override
	public boolean deleteCustomer(Long customerId) {
		CustomerEntity customer = getCustomerDao().find(customerId);
		getCustomerDao().delete(customer);
		LOG.debug("The customer with id '{}' has been deleted.", customerId);
		return true;
	}

	@Override
	public CustomerEto saveCustomer(CustomerEto customer) {
		Objects.requireNonNull(customer, "customer");
		CustomerEntity customerEntity = getBeanMapper().map(customer, CustomerEntity.class);

		// initialize, validate customerEntity here if necessary
		CustomerEntity resultEntity = getCustomerDao().save(customerEntity);
		LOG.debug("Customer with id '{}' has been created.", resultEntity.getId());

		return getBeanMapper().map(resultEntity, CustomerEto.class);
	}

	/**
	 * Returns the field 'customerDao'.
	 *
	 * @return the {@link CustomerDao} instance.
	 */
	public CustomerDao getCustomerDao() {

		return this.customerDao;
	}

	@Override
	public CustomerCto findCustomerCto(Long id) {
		LOG.debug("Get CustomerCto with id {} from database.", id);
		CustomerEntity entity = getCustomerDao().findOne(id);
		CustomerCto cto = new CustomerCto();
		cto.setCustomer(getBeanMapper().map(entity, CustomerEto.class));

		return cto;
	}

	@Override
	public PaginatedListTo<CustomerCto> findCustomerCtos(CustomerSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<CustomerEntity> customers = getCustomerDao().findCustomers(criteria);
		List<CustomerCto> ctos = new ArrayList<>();
		for (CustomerEntity entity : customers.getResult()) {
			CustomerCto cto = new CustomerCto();
			cto.setCustomer(getBeanMapper().map(entity, CustomerEto.class));
			ctos.add(cto);

		}
		PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
		PaginatedListTo<CustomerCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
		return pagListTo;
	}

	@Override
	public EntertainmentEto findEntertainment(Long id) {
		LOG.debug("Get Entertainment with id {} from database.", id);
		return getBeanMapper().map(getEntertainmentDao().findOne(id), EntertainmentEto.class);
	}

	@Override
	public PaginatedListTo<EntertainmentEto> findEntertainmentEtos(EntertainmentSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<EntertainmentEntity> entertainments = getEntertainmentDao().findEntertainments(criteria);
		return mapPaginatedEntityList(entertainments, EntertainmentEto.class);
	}

	@Override
	public boolean deleteEntertainment(Long entertainmentId) {
		EntertainmentEntity entertainment = getEntertainmentDao().find(entertainmentId);
		getEntertainmentDao().delete(entertainment);
		LOG.debug("The entertainment with id '{}' has been deleted.", entertainmentId);
		return true;
	}

	@Override
	public EntertainmentEto saveEntertainment(EntertainmentEto entertainment) {
		Objects.requireNonNull(entertainment, "entertainment");
		EntertainmentEntity entertainmentEntity = getBeanMapper().map(entertainment, EntertainmentEntity.class);

		// initialize, validate entertainmentEntity here if necessary
		EntertainmentEntity resultEntity = getEntertainmentDao().save(entertainmentEntity);
		LOG.debug("Entertainment with id '{}' has been created.", resultEntity.getId());

		return getBeanMapper().map(resultEntity, EntertainmentEto.class);
	}

	/**
	 * Returns the field 'entertainmentDao'.
	 *
	 * @return the {@link EntertainmentDao} instance.
	 */
	public EntertainmentDao getEntertainmentDao() {

		return this.entertainmentDao;
	}

	@Override
	public EntertainmentCto findEntertainmentCto(Long id) {
		LOG.debug("Get EntertainmentCto with id {} from database.", id);
		EntertainmentEntity entity = getEntertainmentDao().findOne(id);
		EntertainmentCto cto = new EntertainmentCto();
		cto.setEntertainment(getBeanMapper().map(entity, EntertainmentEto.class));

		return cto;
	}

	@Override
	public PaginatedListTo<EntertainmentCto> findEntertainmentCtos(EntertainmentSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<EntertainmentEntity> entertainments = getEntertainmentDao().findEntertainments(criteria);
		List<EntertainmentCto> ctos = new ArrayList<>();
		for (EntertainmentEntity entity : entertainments.getResult()) {
			EntertainmentCto cto = new EntertainmentCto();
			cto.setEntertainment(getBeanMapper().map(entity, EntertainmentEto.class));
			ctos.add(cto);

		}
		PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
		PaginatedListTo<EntertainmentCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
		return pagListTo;
	}

	@Override
	public EntertainmentReservationEto findReservation(Long id) {
		LOG.debug("Get Reservation with id {} from database.", id);
		return getBeanMapper().map(getReservationDao().findOne(id), EntertainmentReservationEto.class);
	}

	@Override
	public PaginatedListTo<EntertainmentReservationEto> findReservationEtos(EntertainmentReservationSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<EntertainmentReservationEntity> reservations = getReservationDao().findReservations(criteria);
		return mapPaginatedEntityList(reservations, EntertainmentReservationEto.class);
	}

	@Override
	public boolean deleteReservation(Long reservationId) {
		EntertainmentReservationEntity reservation = getReservationDao().find(reservationId);
		getReservationDao().delete(reservation);
		LOG.debug("The reservation with id '{}' has been deleted.", reservationId);
		return true;
	}

	@Override
	public EntertainmentReservationEto saveReservation(EntertainmentReservationEto reservation) {
		Objects.requireNonNull(reservation, "reservation");
		EntertainmentReservationEntity reservationEntity = getBeanMapper().map(reservation, EntertainmentReservationEntity.class);

		reservationEntity.setCustomerid(getCustomerDao().find(reservation.getCustomeridId()));
		reservationEntity.setShowid(getShowDetailsDao().find(reservation.getShowidId()));
		
		reservationEntity.setBookingDate(new Date(System.currentTimeMillis()));
		
		// initialize, validate reservationEntity here if necessary
		EntertainmentReservationEntity resultEntity = getReservationDao().save(reservationEntity);
		LOG.debug("Reservation with id '{}' has been created.", resultEntity.getId());

		return getBeanMapper().map(resultEntity, EntertainmentReservationEto.class);
	}

	/**
	 * Returns the field 'reservationDao'.
	 *
	 * @return the {@link EntertainmentReservationDao} instance.
	 */
	public EntertainmentReservationDao getReservationDao() {

		return this.reservationDao;
	}

	@Override
	public EntertainmentReservationCto findReservationCto(Long id) {
		LOG.debug("Get ReservationCto with id {} from database.", id);
		EntertainmentReservationEntity entity = getReservationDao().findOne(id);
		EntertainmentReservationCto cto = new EntertainmentReservationCto();
		cto.setReservation(getBeanMapper().map(entity, EntertainmentReservationEto.class));
		cto.setCustomerid(getBeanMapper().map(entity.getCustomerid(), CustomerEto.class));
		cto.setShowid(getBeanMapper().map(entity.getShowid(), ShowDetailsEto.class));

		return cto;
	}

	@Override
	public PaginatedListTo<EntertainmentReservationCto> findReservationCtos(EntertainmentReservationSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<EntertainmentReservationEntity> reservations = getReservationDao().findReservations(criteria);
		List<EntertainmentReservationCto> ctos = new ArrayList<>();
		for (EntertainmentReservationEntity entity : reservations.getResult()) {
			EntertainmentReservationCto cto = new EntertainmentReservationCto();
			cto.setReservation(getBeanMapper().map(entity, EntertainmentReservationEto.class));
			cto.setCustomerid(getBeanMapper().map(entity.getCustomerid(), CustomerEto.class));
			cto.setShowid(getBeanMapper().map(entity.getShowid(), ShowDetailsEto.class));
			ctos.add(cto);

		}
		PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
		PaginatedListTo<EntertainmentReservationCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
		return pagListTo;
	}

	@Override
	public ShowDetailsEto findShowDetails(Long id) {
		LOG.debug("Get ShowDetails with id {} from database.", id);
		return getBeanMapper().map(getShowDetailsDao().findOne(id), ShowDetailsEto.class);
	}

	@Override
	public PaginatedListTo<ShowDetailsEto> findShowDetailsEtos(ShowDetailsSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<ShowDetailsEntity> showdetailss = getShowDetailsDao().findShowDetailss(criteria);
		return mapPaginatedEntityList(showdetailss, ShowDetailsEto.class);
	}

	@Override
	public boolean deleteShowDetails(Long showDetailsId) {
		ShowDetailsEntity showDetails = getShowDetailsDao().find(showDetailsId);
		getShowDetailsDao().delete(showDetails);
		LOG.debug("The showDetails with id '{}' has been deleted.", showDetailsId);
		return true;
	}

	@Override
	public ShowDetailsEto saveShowDetails(ShowDetailsEto showDetails) {
		Objects.requireNonNull(showDetails, "showDetails");
		ShowDetailsEntity showDetailsEntity = getBeanMapper().map(showDetails, ShowDetailsEntity.class);

		// initialize, validate showDetailsEntity here if necessary
		ShowDetailsEntity resultEntity = getShowDetailsDao().save(showDetailsEntity);
		LOG.debug("ShowDetails with id '{}' has been created.", resultEntity.getId());

		return getBeanMapper().map(resultEntity, ShowDetailsEto.class);
	}

	/**
	 * Returns the field 'showDetailsDao'.
	 *
	 * @return the {@link ShowDetailsDao} instance.
	 */
	public ShowDetailsDao getShowDetailsDao() {

		return this.showDetailsDao;
	}

	@Override
	public ShowDetailsCto findShowDetailsCto(Long id) {
		LOG.debug("Get ShowDetailsCto with id {} from database.", id);
		ShowDetailsEntity entity = getShowDetailsDao().findOne(id);
		ShowDetailsCto cto = new ShowDetailsCto();
		cto.setShowDetails(getBeanMapper().map(entity, ShowDetailsEto.class));
		cto.setEntertainmentId(getBeanMapper().map(entity.getEntertainmentId(), EntertainmentEto.class));

		return cto;
	}

	@Override
	public PaginatedListTo<ShowDetailsCto> findShowDetailsCtos(ShowDetailsSearchCriteriaTo criteria) {
		criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
		PaginatedListTo<ShowDetailsEntity> showdetailss = getShowDetailsDao().findShowDetailss(criteria);
		List<ShowDetailsCto> ctos = new ArrayList<>();
		for (ShowDetailsEntity entity : showdetailss.getResult()) {
			ShowDetailsCto cto = new ShowDetailsCto();
			cto.setShowDetails(getBeanMapper().map(entity, ShowDetailsEto.class));
			cto.setEntertainmentId(getBeanMapper().map(entity.getEntertainmentId(), EntertainmentEto.class));
			ctos.add(cto);

		}
		PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
		PaginatedListTo<ShowDetailsCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
		return pagListTo;
	}

}
