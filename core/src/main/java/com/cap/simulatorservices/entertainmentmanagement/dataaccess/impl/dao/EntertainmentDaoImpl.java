package com.cap.simulatorservices.entertainmentmanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.EntertainmentEntity;
import com.cap.simulatorservices.entertainmentmanagement.dataaccess.api.dao.EntertainmentDao;
import com.cap.simulatorservices.entertainmentmanagement.logic.api.to.EntertainmentSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link EntertainmentDao}.
 */
@Named
public class EntertainmentDaoImpl extends ApplicationDaoImpl<EntertainmentEntity> implements EntertainmentDao {

	/**
	 * The constructor.
	 */
	public EntertainmentDaoImpl() {

		super();
	}

	@Override
	public Class<EntertainmentEntity> getEntityClass() {
		return EntertainmentEntity.class;
	}

	@Override
	public PaginatedListTo<EntertainmentEntity> findEntertainments(EntertainmentSearchCriteriaTo criteria) {

		EntertainmentEntity entertainment = Alias.alias(EntertainmentEntity.class);
		EntityPathBase<EntertainmentEntity> alias = Alias.$(entertainment);
		JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

		String address = criteria.getAddress();
		if (address != null) {
			query.where(Alias.$(entertainment.getAddress()).eq(address));
		}
		Integer zip = criteria.getZip();
		if (zip != null) {
			query.where(Alias.$(entertainment.getZip()).eq(zip));
		}
		String entertainmentType = criteria.getEntertainmentType();
		if (entertainmentType != null) {
			query.where(Alias.$(entertainment.getEntertainmentType()).eq(entertainmentType));
		}
		addOrderBy(query, alias, entertainment, criteria.getSort());

		return findPaginated(criteria, query, alias);
	}

	private void addOrderBy(JPAQuery query, EntityPathBase<EntertainmentEntity> alias,
			EntertainmentEntity entertainment, List<OrderByTo> sort) {
		if (sort != null && !sort.isEmpty()) {
			for (OrderByTo orderEntry : sort) {
				switch (orderEntry.getName()) {
				case "address":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(entertainment.getAddress()).asc());
					} else {
						query.orderBy(Alias.$(entertainment.getAddress()).desc());
					}
					break;
				case "zip":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(entertainment.getZip()).asc());
					} else {
						query.orderBy(Alias.$(entertainment.getZip()).desc());
					}
					break;
				case "entertainmentType":
					if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
						query.orderBy(Alias.$(entertainment.getEntertainmentType()).asc());
					} else {
						query.orderBy(Alias.$(entertainment.getEntertainmentType()).desc());
					}
					break;
				}
			}
		}
	}

}