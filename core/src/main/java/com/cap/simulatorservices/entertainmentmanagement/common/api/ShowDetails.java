package com.cap.simulatorservices.entertainmentmanagement.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ShowDetails extends ApplicationEntity {

	public Timestamp getShowTime();

	public void setShowTime(Timestamp showTime);

	public String getClassType();

	public void setClassType(String classType);

	public Long getEntertainmentIdId();

	public void setEntertainmentIdId(Long entertainmentIdId);

}
